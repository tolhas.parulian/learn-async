package manual;

import java.util.function.Supplier;

public class SupplierFactorialTask implements Supplier<Integer> {

    private int number;

    public SupplierFactorialTask(int number) throws InterruptedException {
        this.number = number;
    }

    @Override
    public Integer get(){
        int fact = 1;

        for (int count = number; count > 1 ; count--) {
            fact = fact * count;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return fact;
    }
}

