package quiz;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.function.Consumer;

public class AsyncSearchSimulation {
    public static void main(String[] args){
        ExecutorService executorService = Executors.newFixedThreadPool(5);
        Consumer<Integer> isFound = result -> {
            String returnedString = "";

            if(result < 0){
                System.out.println("Number not found at the List");
            }else{
                System.out.println("Found at index-" + result.toString() + "th");
            }
        };

        List<Integer> searchableArray = Arrays.asList(22, 35, 50, 24, 48, 9, 11, 1, 31, 5, 40, 20, 25, 19, 14, 18, 23, 2, 13, 17, 12, 10, 29, 36, 30, 38, 37, 32, 42, 49, 47, 4, 6, 33, 27, 7, 46, 34, 15, 3, 44, 26, 16, 21, 43, 39, 8, 41, 28, 45);

        CompletableFuture<Integer> firstSearch = CompletableFuture.supplyAsync(new LinearSearchSupplier(51, searchableArray), executorService);
        CompletableFuture<Integer> secondSearch = CompletableFuture.supplyAsync(new LinearSearchSupplier(6, searchableArray), executorService);
        CompletableFuture<Integer> thirdSearch = CompletableFuture.supplyAsync(new LinearSearchSupplier(0, searchableArray), executorService);

        try {
            firstSearch.get(40, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            e.printStackTrace();
        }

        Integer secondSearchResult = secondSearch.getNow(32);
        System.out.println("Found at index-" + secondSearchResult.toString() + "th");

        thirdSearch.thenAccept(isFound);
    }
}
